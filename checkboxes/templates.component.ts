import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-templates',
  templateUrl: './templates.component.html',
  styleUrls: ['./templates.component.css']
})
export class TemplatesComponent implements OnInit {
  templates: Template[];
  templateRevision: Template[];
  insertions: Insertion[];
  networks;

  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.dataService.getTemplates().subscribe((templates: Template[]) => {
      this.templates = templates;
    });
  }

  onTemplateChange(template: Template) {
    this.dataService.getTemplateRevisions(template.scriptSourceId).subscribe((templates) => {
      this.templateRevision = templates;
    });
  }

  onRevChange(scriptId: number) {
    this.dataService.getInsertionsByScriptId(scriptId).subscribe((insertions: Insertion[]) => {
      this.insertions = insertions;
      this.networks = [
        {
          id: 1,
          insertions: [{id: 1}, {id: 2}, {id: 3}]
        },
        {
          id: 2,
          insertions: [{id: 4}, {id: 5}, {id: 7}]
        },
        {
          id: 3,
          insertions: [{id: 7}, {id: 8}, {id: 9}, {id: 10}]
        }
      ];
    });
  }

}

interface Template {
  scriptId: number;
  name: string;
  revInfo: string;
  scriptSourceId: number;
  deprecated: boolean;
  rev: number;
}

interface Insertion {
  networkId: number;
  networkName: string;
  scriptId: number;
  insertionId: number;
  insertionName: string;
  isOnline: boolean;
  insertionStartDate: Date;
  insertionEndDate: Date;
  insertionVolume: number;
}
