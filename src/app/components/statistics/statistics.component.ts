import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.css']
})
export class StatisticsComponent implements OnInit {
  templates: Template[];
  templateRevision: Template[];
  periods: String[];
  frequency: String[];

  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.dataService.getTemplates().subscribe((templates: Template[]) => {
      this.templates = templates;
    });
  }

  onTemplateChange(template: Template) {
    this.dataService.getTemplateRevisions(template.scriptSourceId).subscribe((templates) => {
      this.templateRevision = templates;
    });
  }

  onRevChange(scriptId: number) {
    this.periods = [
      'Yesterday',
      'Previous week',
      'Previous month',
      'Current month'
    ];
    const periodInput = document.getElementById('period');
    periodInput.style.display = 'block';
  }

  onPeriodChange(period: string) {
    this.frequency = [
      'Per hour',
      'Per day',
      'Per week',
      'Per month'
    ];
    const frequencyInput = document.getElementById('frequency');
    frequencyInput.style.display = 'block';
  }

  onTimeChange (time: string) {
    document.getElementById('chart-generator').style.display = 'block';
  }
}

interface Template {
  scriptId: number;
  name: string;
  revInfo: string;
  scriptSourceId: number;
  deprecated: boolean;
  rev: number;
}

interface Insertion {
  networkId: number;
  networkName: string;
  scriptId: number;
  insertionId: number;
  insertionName: string;
  isOnline: boolean;
  insertionStartDate: Date;
  insertionEndDate: Date;
  insertionVolume: number;
}
