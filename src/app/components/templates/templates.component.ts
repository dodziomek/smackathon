import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';
import { forEach } from '@angular/router/src/utils/collection';

@Component({
  selector: 'app-templates',
  templateUrl: './templates.component.html',
  styleUrls: ['./templates.component.css']
})
export class TemplatesComponent implements OnInit {
  templates: Template[];
  templateRevision: Template[];
  networks: Network[];
  selectedNetworks: number[] = [];
  showPutOnlineButton = false;

  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.dataService.getTemplates().subscribe((templates: Template[]) => {
      this.templates = templates;
    });
  }

  onTemplateChange(template: Template) {
    this.dataService.getTemplateRevisions(template.scriptSourceId).subscribe((templates) => {
      this.templateRevision = templates;
      this.networks = null;
    });
  }

  onRevChange(scriptId: number) {
    this.dataService.getInsertionsByScriptId(scriptId).subscribe((insertions: Insertion[]) => {

      this.networks = [];
      for (const ins of insertions) {
        let skip = false;

        for (const net of this.networks) {
          if (net.networkId === ins.networkId) {
            net.insertions.push(ins);
            skip = true;
          }
        }

        if (!skip) {
          this.networks.push({
            networkId: ins.networkId,
            networkName: ins.networkName,
            insertions: [ins],
          });
        }
      }
    });
  }

  showUpdateTo(obj) {
    this.selectedNetworks = [];

    for (const [k, v] of Object.entries(obj)) {
      const kk = Number(k);
      if (v && !isNaN(kk)) {
        this.selectedNetworks.push(kk);
      }
    }
  }

  putOnline(v) {
    let updatedInsertion: Insertion = null;

    for (const network of this.networks) {
      for (const selNetwork of this.selectedNetworks) {
        if (selNetwork === network.networkId) {
          for (const insertion of network.insertions) {
            updatedInsertion = insertion;
            updatedInsertion.scriptId = v.selectNewRev;
            this.dataService.updateInsertion(updatedInsertion)
            .subscribe(
              // () => console.log('sukces');
            );
          }
          // const i = this.networks.indexOf(network);
          // this.networks.splice(i, 1);
        }
      }
    }
    // this.templates = null;
    // this.templateRevision = null;
    // this.networks = null;
    // this.selectedNetworks = null;
    // this.showPutOnlineButton = false;
  }

  onNewRevChange() {
    this.showPutOnlineButton = true;
  }

}

interface Network {
  networkId: number;
  networkName: string;
  insertions: Insertion[];
}

interface Template {
  scriptId: number;
  name: string;
  revInfo: string;
  scriptSourceId: number;
  deprecated: boolean;
  rev: number;
}

interface Insertion {
  id: number;
  networkId: number;
  networkName: string;
  scriptId: number;
  insertionId: number;
  insertionName: string;
  isOnline: boolean;
  insertionStartDate: Date;
  insertionEndDate: Date;
  insertionVolume: number;
}
