import { Component } from '@angular/core';

@Component({
  selector: 'app-chart',
  templateUrl:  './chart.component.html',
  styleUrls: ['./chart.component.css']
})

export class ChartComponent {
  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true,
    legend: {
      position: 'right',
      labels: {
        fontSize: 16,
        padding: 20
      }
    },
    scales: {
      yAxes: [{
        id: 'left-y-axis',
        type: 'linear',
        scaleLabel: {
          display: true,
          labelString: 'Ad impressions',
          fontSize: 14,
          fontStyle: 'bold'
        },
        position: 'left'
      }, {
        id: 'right-y-axis',
        type: 'linear',
        scaleLabel: {
          display: true,
          labelString: 'CTR',
          fontSize: 14,
          fontStyle: 'bold'
        },
        position: 'right'
      }]
    }
  };
  public barChartLabels: string[] = ['Week 14', 'Week 15', 'Week 16', 'Week 17', 'Week 18', 'Week 19', 'Week 20'];
  public barChartType = 'bar';
  public barChartLegend = true;
  public barChartData: any[] = [
    {
      label: 'Fullscreen',
      type: 'line',
      hidden: true,
      data: [32, 25, 33, 88, 12, 92, 33]
    }, {
      label: 'Counter',
      type: 'line',
      hidden: false,
      data: [65, 59, 4, 81, 56, 55, 40]
    }, {
      label: 'Close button',
      type: 'line',
      hidden: true,
      data: [25, 19, 45, 21, 36, 25, 60]
    }, {
      label: 'Custom logo',
      type: 'line',
      hidden: true,
      data: [25, 19, 45, 21, 36, 25, 60]
    }, {
      label: 'Animation',
      type: 'line',
      hidden: true,
      data: [25, 19, 45, 21, 36, 25, 60]
    }, {
      label: 'Floating',
      type: 'line',
      hidden: true,
      data: [25, 19, 45, 21, 36, 25, 60]
    }, {
      label: 'Delay',
      type: 'line',
      hidden: true,
      data: [25, 19, 45, 21, 36, 25, 60]
    }, {
      label: 'Impressions',
      type: 'bar',
      yAxisID: 'left-y-axis',
      data: [69, 59, 4, 81, 56, 55, 40]
    }, {
      label: 'CTR',
      type: 'bar',
      yAxisID: 'right-y-axis',
      data: [0.34, 0.13, 0.25, 0.41, 0.32, 0.18, 0.52]
    }
  ];

  public chartColors: Array<any> = [
    {
      backgroundColor: 'rgba(0,0,0,0)',
      borderColor: 'rgba(44, 62, 80, 0.9)',
      pointBackgroundColor: 'rgba(77,83,96,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(77,83,96,1)'
    },
    {
      backgroundColor: 'rgba(0,0,0,0)',
      borderColor: 'rgba(209, 31, 31, 0.9)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    {
      backgroundColor: 'rgba(0,0,0,0)',
      borderColor: 'rgba(142, 68, 173, 0.9)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    {
      backgroundColor: 'rgba(0,0,0,0)',
      borderColor: 'rgba(41, 128, 185, 0.9)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // Animation
      backgroundColor: 'rgba(0,0,0,0)',
      borderColor: 'rgba(26, 188, 156, 0.9)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // Floating
      backgroundColor: 'rgba(0,0,0,0)',
      borderColor: 'rgba(241, 196, 15, 0.9)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // Delay
      backgroundColor: 'rgba(0,0,0,0)',
      borderColor: 'rgba(106, 136, 36, 0.9)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    {
      backgroundColor: 'rgba(243, 156, 18, 0.6)'
      /*borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'*/
    },
    {
      backgroundColor: 'rgba(127, 140, 141, 0.6)'
      /*borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'*/
    }
  ];

  public getRandom(e: number, div: number): number {
    return (Math.round(Math.random() * 100) / e) * div;
  }

  public randomize(): void {
    const chart = document.getElementById('chart');
    chart.style.display = 'block';
    const chartWrapper = document.getElementById('chart-wrapper');
    chartWrapper.style.padding = '20px';
    const inputs = document.getElementsByClassName('input-section') as any;
    for (let i = 0; i < inputs.length; i++) {
      // console.log(inputs[i]);
      inputs[i].style.float = 'left';
      inputs[i].style.width = '25%';
    }

    const xSize = this.barChartLabels.length;
    const ySize = 9; // number of parameters
    const clone = JSON.parse(JSON.stringify(this.barChartData));
    let div = 1;
    for (let _j = 0; _j < ySize; _j++) {
      const arr = [];
      div = (_j < 7) ? 0.8 : 1;
      for (let _i = 0; _i < xSize; _i++) {
        arr.push(this.getRandom(1, div));
      }
      clone[_j].data = arr;
    }
    clone[ySize - 1].data = [
      this.getRandom(100, 1),
      this.getRandom(100, 1),
      this.getRandom(100, 1),
      this.getRandom(100, 1),
      this.getRandom(100, 1),
      this.getRandom(100, 1),
      this.getRandom(100, 1)
    ];
    this.barChartData = clone;
  }

  public chartClicked(e: any): void {}
  public chartHovered(e: any): void {}
}
