import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { TemplatesComponent } from './components/templates/templates.component';
import { StatisticsComponent } from './components/statistics/statistics.component';

import { DataService } from './services/data.service';

// web service moks
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { DbService } from './services/db.service';

import { NavbarComponent } from './components/navbar/navbar.component';
import { ChartComponent } from './components/chart/chart.component';
import { ChartsModule } from 'ng2-charts';

const appRoutes: Routes = [
  {path: '', component: TemplatesComponent},
  {path: 'stats', component: StatisticsComponent},
];

@NgModule({
  declarations: [
    AppComponent,
    TemplatesComponent,
    StatisticsComponent,
    NavbarComponent,
    ChartComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    HttpModule,
    InMemoryWebApiModule.forRoot(DbService),
    FormsModule,
    ChartsModule
  ],
  providers: [ DataService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
