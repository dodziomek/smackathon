import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

// import { Observable } from 'rxjs/Observable';
// import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
// import { catchError, map, tap } from 'rxjs/operators';

@Injectable()
export class DataService {

  constructor(public http: Http) { }

  getTemplates() {
    return this.http.get('api/templates?deprecated=false').map((res) => {
      const result = JSON.parse(res.text());
      return result.sort((a, b) => {
        return a.name < b.name ? -1 : 1;
      });
    });
  }

  getTemplateRevisions(scriptSourceId: number) {
    return this.http.get(`api/templates?scriptSourceId=${scriptSourceId}`).map((res) => {
      const result = JSON.parse(res.text());
      return result.sort((a, b) => {
        return a.rev > b.rev ? -1 : 1;
      });
    });
  }

  getInsertionsByScriptId(scriptId: number) {
    return this.http.get(`api/insertions?scriptId=${scriptId}`)
    .map(res => res.json());
  }

  updateInsertion(insertion: any) {
    const insertionHolder = JSON.stringify(insertion);
    return this.http.put(`api/insertions`, insertionHolder);
  }
}
